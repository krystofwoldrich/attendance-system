import { createBrowserHistory } from "history";
import { routerMiddleware } from 'connected-react-router';
import { configureStore, getDefaultMiddleware, Action } from '@reduxjs/toolkit';
import { ThunkAction as GenericThunkAction, ThunkDispatch as GenericThunkDispatch } from 'redux-thunk'
import { createRouterReducer, IState } from './reducer';
import { HistoryLocationChanged } from './History/historyActions';

export const history = createBrowserHistory();

export function configureAppStore() {
	const store = configureStore({
		reducer: createRouterReducer(history),
		middleware: [
			...getDefaultMiddleware({
				thunk: true,
				immutableCheck: false,
				serializableCheck: true,
			}),
			routerMiddleware(history),
		],
	});

	history.listen((location) => {
		store.dispatch<HistoryLocationChanged>({
			type: HistoryLocationChanged,
			location,
		});
	});

	return store;
}


export type ThunkAction = GenericThunkAction<void, IState, unknown, Action<string>>;
export type ThunkDispatch = GenericThunkDispatch<IState, unknown, Action<string>>;
