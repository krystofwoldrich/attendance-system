
export const ApiBaseUrlChange = 'Api.BaseUrl.Change';
export interface ApiBaseUrlChange {
	type: typeof ApiBaseUrlChange;
	baseUrl: string;
}

type ApiActions = ApiBaseUrlChange;

export default ApiActions;
