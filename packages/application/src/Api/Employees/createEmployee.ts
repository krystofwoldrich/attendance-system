import {
	EmployeeToCreate,
	Employee,
} from 'domain-model/dist/model/Employee/EmployeeAttributes';
import {
	doPost,
} from '../../Api/doRequest';

export const RESOURCE = 'employees';

export default function createEmployee(baseUrl: string, data: EmployeeToCreate) {
	const url = `${baseUrl}/${RESOURCE}`;
	return doPost<Employee, EmployeeToCreate>(url, data);
}
