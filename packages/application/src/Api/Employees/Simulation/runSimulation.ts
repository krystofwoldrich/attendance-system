
import { RESOURCE as EMPLOYEE_RESOURCE } from '../createEmployee';
import { doPost } from '../../doRequest';

const RESOURCE = `simulation/run`;

export function getResource(employeeId: number) {
	return `${EMPLOYEE_RESOURCE}/${employeeId}/${RESOURCE}`;
}

export default function runSimulation(baseUrl: string, data: SimulationOptions) {
	const resource = getResource(data.employeeId)
	const url = `${baseUrl}/${resource}`;
return doPost<void, SimulationOptions>(url, data, true);
}

export interface SimulationOptions {
	employeeId: number,
	projectAssignmentId: number,
	from: Date,
	to: Date,
}
