import { 
	SimulationToCreate,
	Simulation,
} from 'domain-model/dist/model/Simulation/SimulationAttributes';
import { RESOURCE as EMPLOYEE_RESOURCE } from '../createEmployee';
import { doPost } from '../../doRequest';

const RESOURCE = `simulation`;

export function getResource(employeeId: number) {
	return `${EMPLOYEE_RESOURCE}/${employeeId}/${RESOURCE}`;
}

export default function createSimulation(baseUrl: string, data: SimulationToCreate) {
	const resource = getResource(data.employeeId)
	const url = `${baseUrl}/${resource}`;
	return doPost<Simulation, SimulationToCreate>(url, data);
}
