import {
	ContractToCreate,
	Contract,
} from 'domain-model/dist/model/Contract/ContractAttributes';
import { RESOURCE as EMPLOYEE_RESOURCE } from '../createEmployee';
import { doPost } from '../../doRequest';

const RESOURCE = `contracts`;

export function getResource(employeeId: number) {
	return `${EMPLOYEE_RESOURCE}/${employeeId}/${RESOURCE}`;
}

export default function createSimulation(baseUrl: string, data: ContractToCreate) {
	const resource = getResource(data.employeeId)
	const url = `${baseUrl}/${resource}`;
	return doPost<Contract, ContractToCreate>(url, data);
}
