
export default async function doRequest<T, S = any>(url: string, method: string, body?: S, raw?: boolean) {
	const response = await fetch(
		url,
		{ 
			method,
			body: JSON.stringify(body),
			headers: {
				'Content-Type': 'application/json',
			}
		},
	);

	console.log(response);

	if (raw) {
		return response.arrayBuffer() as unknown as T;
	} else {
		const responseJSON = await response.json();
		const data = responseJSON as T;
		return data;
	}
}

export function doGet<T>(url: string) {
	return doRequest<T>(url, 'GET');
}

export function doPost<T, S = any>(url: string, body: S, raw?: boolean) {
	return doRequest<T, S>(url, 'POST', body, raw);
}

export function doPut<T, S = any>(url: string, body: S) {
	return doRequest<T, S>(url, 'PUT', body);
}

export function doDelete<T, S = any>(url: string, body: S) {
	return doRequest<T, S>(url, 'DELETE', body);
}
