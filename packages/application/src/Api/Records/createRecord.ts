import { RecordToCreate, Record } from 'domain-model/dist/model/Record/RecordAttributes';
import { doPost } from '../doRequest';

export const RESOURCE = `records`;

export default function createRecord(baseUrl: string, data: RecordToCreate) {
	const url = `${baseUrl}/${RESOURCE}`;
	return doPost<Record, RecordToCreate>(url, data);
}
