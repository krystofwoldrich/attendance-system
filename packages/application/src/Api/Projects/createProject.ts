import { ProjectToCreate, Project } from 'domain-model/dist/model/Project/ProjectAttributes';
import { doPost } from '../doRequest';

export const RESOURCE = `projects`;

export default function createProject(baseUrl: string, data: ProjectToCreate) {
	const url = `${baseUrl}/${RESOURCE}`;
	return doPost<Project, ProjectToCreate>(url, data);
}
