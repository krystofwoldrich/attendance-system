import { ProjectAssignment, ProjectAssignmentToCreate } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { RESOURCE as PROJECTS_RESOURCE } from './createProject';
import {
	doPost,
} from '../../Api/doRequest';

const RESOURCE = `assignments`;

export function getResource(projectId: number) {
	return `${PROJECTS_RESOURCE}/${projectId}/${RESOURCE}`;
}

export default function createProjectAssignment(baseUrl: string, data: ProjectAssignmentToCreate) {
	const url = `${baseUrl}/${getResource(data.projectId)}`;
	return doPost<ProjectAssignment, ProjectAssignmentToCreate>(url, data);
}
