import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { Project } from 'domain-model/dist/model/Project/ProjectAttributes';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { IState } from '../../reducer';
import { ProjectAssignment, ProjectAssignmentAttributes } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { ThunkDispatch } from '../../store';
import { EmployeesLoad } from '../../Employees/employeesThunk';
import { ProjectsLoad } from '../../Projects/projectsThunk';
import { Chip } from '@material-ui/core';
import { getProjectAssignmentsMapByContractIds, getEmployeeAssignments, getEmployeeProjects } from '../../Projects/helper';

const TITLE_LABEL = 'Přehled zaměstnanců';
const EMPLOYEE_COLUMN_LABEL = 'Zaměstnanec';
const PROJECTS_COLUMN_LABEL = 'Projekty';
const CONTRACT_COLUMN_LABEL = 'Úvazek';
const HOURS_COLUMN_LABEL = 'Odpracované hodiny';

export interface I_ROW {
	id: number;
	employeeName: string;
	projects: {
		id: number;
		name: string;
	}[];
	extent: number;
	hours: number;
}

const getColumns = (): Column<I_ROW>[] => {
	return [
		{ title: EMPLOYEE_COLUMN_LABEL, field: 'employeeName' },
		{ 
			title: PROJECTS_COLUMN_LABEL,
			field: 'projects',
			render: data => renderProjectsChips(data.projects),
		},
		{ title: CONTRACT_COLUMN_LABEL, field: 'extent' },
		{ title: HOURS_COLUMN_LABEL, field: 'hours' },
	];
};

export interface IStateProps {
	data: I_ROW[];
}

export type IProps = IStateProps;

export function EmployeesTable(props: IProps) {
	const history = useHistory();
	const { data } = props;
	const columns = getColumns();

	const onRowClick = (_?: React.MouseEvent, data?: I_ROW) => {
		history.push(`/employee/${data?.id}`);
	}

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			onRowClick={onRowClick}
		/>
	);
}

export default connect(
	(state: IState): IStateProps => {
		const projects = Object.values(state.projects.data);
		const assignments = projects.reduce<ProjectAssignment[]>((prev, curr) => ([...prev, ...curr.projectAssignments]), []);
		const assignmentsMapByContractId = getProjectAssignmentsMapByContractIds(assignments);

		const employeeTableData = state.employees.data.map((employee) => {
			
			const employeeAssignments = employee.contracts
				? getEmployeeAssignments(assignmentsMapByContractId, employee.contracts)
				: [];
			const employeeProjects = getEmployeeProjects(employeeAssignments, state.projects.data);

			const extent = employee.contracts!.reduce<number>((sum, contract) => sum + contract.extent, 0)

			return {
				id: employee.id!,
				employeeName: employee.name,
				projects: Object.values(employeeProjects || []),
				extent,
				hours: 0,
			};
		});

		return {
			data: employeeTableData,
		};
	},
	(dispatch: ThunkDispatch): {} => {
		dispatch(EmployeesLoad());
		dispatch(ProjectsLoad());
		return {};
	},
)(EmployeesTable);

export function renderProjectsChips(
	data: {
		id: number;
		name: string;
	}[],
) {
	const chips = data.map((project) => <Chip label={project.name} color='secondary' key={project.id}/>);
	return (
		<React.Fragment>
			{chips}
		</React.Fragment>
	);
}
