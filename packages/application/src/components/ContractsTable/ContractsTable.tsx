import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { ContractToCreate } from 'domain-model/dist/model/Contract/ContractAttributes';
import { ContractType } from 'domain-model/dist/model/Contract/ContractType';

const TITLE_LABEL = 'Úvazky';
const INDEX_LABEL = 'Číslo';
const TYPE_LABEL = 'Typ';
const EXTENT_LABEL = 'Velikost';
const FROM_DATE_LABEL = 'Od';
const TO_DATE_LABEL = 'Do';

export interface I_ROW {
	id: number;
	type: string | undefined;
	extent: string | undefined;
	from: Date | string | undefined;
	to: Date | string | undefined;
}

const getColumns = (dataLength: number): Column<I_ROW>[] => {
	const contractTypesLookup = {
		"Administrative": "Administrative",
		"Pedagogue": "Pedagogue",
		"Scientist": "Scientist",
		"Technician": "Technician",
	};

	return [
		{ title: INDEX_LABEL, field: 'id', type: 'numeric', editable: 'never' },
		{ title: TYPE_LABEL, field: 'type', lookup: contractTypesLookup },
		{ title: EXTENT_LABEL, field: 'extent', type: 'numeric' },
		{ title: FROM_DATE_LABEL, field: 'from', type: 'date' },
		{ title: TO_DATE_LABEL, field: 'to', type: 'date' },
	];
};

export interface IProps {
	data: I_ROW[];
	create: (data: Partial<ContractToCreate>) => void;
	update: (id: number, data: Partial<ContractToCreate>) => void;
	remove: (id: number) => void;
}

export default function ContractsTable(props: IProps) {
	const { data, create, update, remove } = props;
	const columns = getColumns(data.length);

	const onRowAdd = async (newData: I_ROW) => {
		console.log('onRowAdd', 'newData', newData);
		const Contract = convert(newData);
		create(Contract);
	};

	const onRowUpdate = async (newData: I_ROW, oldData?: I_ROW) => {
		console.log('onRowUpdate', 'newData', newData);
		console.log('oldData', oldData);
		const contract = convert(newData);
		update(newData.id, contract);
	};

	const onRowDelete = async (oldData: I_ROW) => {
		console.log('onRowDelete', 'newData', oldData);
		remove(oldData.id);
	};

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			editable={{
				onRowAdd,
				onRowUpdate,
				onRowDelete,
			}}
		/>	
	);
}

export function convert(data: I_ROW): Partial<ContractToCreate> {
	return {
		...data,
		type: data.type as ContractType,
		extent: data.extent ? parseInt(data.extent) : undefined,
		from: typeof data.from === 'string' ? data.from : data.from?.toISOString(),
		to: typeof data.to === 'string' ? data.to : data.to?.toISOString(),
	};
}
