import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Card, Button, InputAdornment } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import {
	MuiPickersUtilsProvider,
	KeyboardDatePicker,
} from '@material-ui/pickers';
import { useForm, Controller } from 'react-hook-form'
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';

import styles from './NewProjectForm.module.css';
import { ProjectToCreate } from 'domain-model/dist/model/Project/ProjectAttributes';
import { NewProjectCreate } from '../../Projects/projectsThunk';
import { useHistory } from 'react-router-dom';
import { PROJECTS_PATH } from '../../Router/routes';

const PROJECT_NAME_LABEL = 'Název projektu';
const PROJECT_DESCRIPTION_LABEL = 'Popis projektu';
const PROJECT_HOURS_LABEL = 'Časová dotace v hodinách';
const PROJECT_FROM_DATE_LABEL = 'Datum zahájení';
const PROJECT_TO_DATE_LABEL = 'Datum ukončení';
const SAVE_BUTTON_LABEL = 'Uložit nový projekt';

export interface IStateProps {

}

export interface IDispatchProps {
	save: (data: ProjectToCreate) => void;
}

export type IProps = IStateProps & IDispatchProps;

export function NewProjectForm(props: IProps) {
	const { save } = props;
	const { handleSubmit, control, errors, triggerValidation, getValues, setValue } = useForm()
	const history = useHistory();

	const onSubmit = (data: any) => {
		console.log(data);
		save({
			name: data.project_name,
			description: data.project_description,
			hours: data.project_hours,
			from: (data.project_from_date as Date).toISOString(),
			to: (data.project_to_date as Date).toISOString(),
		});
		history.push(PROJECTS_PATH);
	}

	const toValidation = (data: any) => {
		if (data && getValues().project_from_date) {
			return isDayOrMoreAfter(data, getValues().project_from_date)
				&& data instanceof Date
				&& !isNaN(data as any);
		} else {
			return false;
		}
	};

	const fromValidation = (data: any) => {
		if (data && getValues().project_to_date) {
			return isDayOrMoreBefore(data, getValues().project_to_date)
				&& data instanceof Date
				&& !isNaN(data as any);
		} else {
			return false;
		}
	}

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Card className={styles.container}>
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<Controller
						name='project_name'
						defaultValue='Nový inovativní projekt'
						rules={{
							required: true,
							minLength: 1,
						}}
						control={control}
						as={
							<TextField
								label={PROJECT_NAME_LABEL}
								id='filled-size-small'
								variant='filled'
								size='small'
								error={errors.project_name ? true : false}
								helperText={errors.project_name ? 'Zadejte název projektu' : undefined}
							/>
						}
					/>
					<Controller
						name='project_description'
						defaultValue=''
						control={control}
						as={
							<TextField
								label={PROJECT_DESCRIPTION_LABEL}
								id='filled-size-small'
								variant='filled'
								size='small'
							/>
						}
					/>
					<Controller
						name='project_hours'
						control={control}
						defaultValue={360}
						rules={{
							validate: (data: any) => {
								return data >= 1;
							}
						}}
						as={
							<TextField
								type='number'
								label={PROJECT_HOURS_LABEL}
								id='filled-size-small'
								variant='filled'
								size='small'
								error={errors.project_hours ? true : false}
								helperText={errors.project_hours ? 'Zadejte hodinovou dotaci rovnou 1 nebo větší' : undefined}				
							/>
						}
					/>
					<Controller
						name='project_from_date'
						control={control}
						defaultValue={new Date()}
						rules={{
							validate: fromValidation,
						}}
						onChange={(data) => {
							setTimeout(async () => await triggerValidation('project_to_date'), 1);
							return data[0];
						}}
						as={
							<KeyboardDatePicker
								disableToolbar
								variant='inline'
								inputVariant='filled'
								size='small'
								format='dd/MM/yyyy'
								margin='normal'
								id='date-picker-inline'
								label={PROJECT_FROM_DATE_LABEL}
								value={getValues().project_from_date}
								onChange={() => {}}
								KeyboardButtonProps={{
									'aria-label': 'change date',
								}}
								error={errors.project_from_date ? true : false}
								helperText={errors.project_from_date ? 'Zadejte datum zahájení před datem ukončení projektu' : undefined}
							/>
						}
					/>
					<Controller
						name='project_to_date'
						control={control}
						defaultValue={new Date()}
						rules={{
							validate: toValidation,
						}}
						onChange={(data) => {
							setTimeout(async () => await triggerValidation('project_from_date'), 1);
							return data[0];
						}}
						as={
							<KeyboardDatePicker
								disableToolbar
								variant='inline'
								inputVariant='filled'
								size='small'
								format='dd/MM/yyyy'
								margin='normal'
								id='date-picker-inline'
								label={PROJECT_TO_DATE_LABEL}
								value={getValues().project_to_date}
								onChange={() => {}}
								KeyboardButtonProps={{
									'aria-label': 'change date',
								}}
								error={errors.project_to_date ? true : false}
								helperText={errors.project_to_date ? 'Zadejte datum ukončení po datu zahájení projektu' : undefined}
							/>
						}
					/>
					<Button variant='contained' color='primary' size='small' type='submit'>{SAVE_BUTTON_LABEL}</Button>
				</MuiPickersUtilsProvider>
			</Card>
		</form>
	);
}

export default connect(
	(state: IState): IStateProps => ({

	}),
	(dispatch: ThunkDispatch): IDispatchProps => {
		return {
			save: (data: ProjectToCreate) => dispatch(NewProjectCreate(data)),
		};
	},
)(NewProjectForm);

export function isDayOrMoreBefore(tested: Date, condition: Date) {
	if (tested.getFullYear() > condition.getFullYear()) {
		return false;
	}
	if (tested.getMonth() > condition.getMonth()) {
		return false;
	}
	if (tested.getDate() >= condition.getDate()) {
		return false;
	}
		
	return true;
}

export function isDayOrMoreAfter(tested: Date, condition: Date) {
	if (tested.getFullYear() < condition.getFullYear()) {
		return false;
	}
	if (tested.getMonth() < condition.getMonth()) {
		return false;
	}
	if (tested.getDate() <= condition.getDate()) {
		return false;
	}
		
	return true;
}