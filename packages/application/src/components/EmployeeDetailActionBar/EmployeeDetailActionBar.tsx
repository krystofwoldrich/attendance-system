import React, { useState } from 'react';
import { connect } from 'react-redux';
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';
import { Button } from '@material-ui/core';
import { GenerateWorksheet } from '../../Worksheets/worksheetsThunk';
import { EmployeesLoad } from '../../Employees/employeesThunk';
import { ProjectsLoad } from '../../Projects/projectsThunk';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import styles from './EmployeeDetailActionBar.module.css';
import EmployeeDetailActionBarRunSimulation from './EmployeeDetailActionBarRunSimulation';

const YEAR_IN_MS = 31556952000;

interface IDispatchProps {
	generateWorksheet: (
		employeeId: number,
		projectId: number,
		from: Date,
		to: Date,
	) => void;
}

export interface IOwnProps {
	employeeId: number | undefined;
	projectId: number | undefined;
}

export type IProps = IOwnProps & IDispatchProps;

export function EmployeeDetailActionBar(props: IProps) {
	const [ from, setFrom ] = useState<Date | null>(new Date(Date.now()-YEAR_IN_MS));
	const [ to, setTo ] = useState<Date | null>(new Date());
	const { generateWorksheet, employeeId, projectId } = props;

	const generateButtonDisabled = from === null || to === null || !projectId || !employeeId;
	const onWorksheetGenerateBtnClick = () => {
		console.log('onWorksheetGenerateBtnClick clicked');
		if (employeeId && projectId && from && to) {
			generateWorksheet(employeeId, projectId, from, to);
		}
	};


	return (
		<div className={styles.container}>
			<div className={styles.actionRow}>
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<KeyboardDatePicker
						disableToolbar
						variant="inline"
						format="dd/MM/yyyy"
						margin="none"
						inputVariant="filled"
						size="small"
						id="date-picker-inline"
						label="Od"
						value={from}
						onChange={(date) => setFrom(date)}
						KeyboardButtonProps={{
							'aria-label': 'change date',
						}}
					/>
					<KeyboardDatePicker
						disableToolbar
						variant="inline"
						format="dd/MM/yyyy"
						margin="none"
						inputVariant="filled"
						size="small"
						id="date-picker-inline"
						label="Do"
						value={to}
						onChange={(date) => setTo(date)}
						KeyboardButtonProps={{
							'aria-label': 'change date',
						}}
					/>
				</MuiPickersUtilsProvider>
				<Button
					size="small"
					disabled={generateButtonDisabled}
					onClick={onWorksheetGenerateBtnClick}
					>
					Generovat přehled
				</Button>
			</div>
			<br/>
			<EmployeeDetailActionBarRunSimulation
				employeeId={employeeId}
				projectId={projectId}
			/>
		</div>
	);
}

export default connect(
	(state: IState, props: IOwnProps): {} => {
		return {};
	},
	(dispatch: ThunkDispatch): IDispatchProps => {
		dispatch(EmployeesLoad());
		dispatch(ProjectsLoad());
		return {
			generateWorksheet: (employeeId: number, projectId: number, from: Date, to: Date) => dispatch(GenerateWorksheet(employeeId, projectId, from, to)),
		};
	},
)(EmployeeDetailActionBar);
 