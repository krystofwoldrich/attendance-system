import React from 'react';
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';
import MaterialTable, { Column } from 'material-table';
import { connect } from 'react-redux';
import { EmployeesLoad } from '../../Employees/employeesThunk';
import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { ProjectAssignment } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { ProjectsLoad } from '../../Projects/projectsThunk';

const TITLE_LABEL = 'Úvazky na projektech';
const NAME_LABEL = 'Jméno zaměstnavatele';
const FROM_LABEL = 'Od';
const TO_LABEL = 'Do';
const EXTENT_LABEL = 'Druh úvazku';

export interface I_ROW {
}

const getColumns = (): Column<I_ROW>[] => {
	return [
		{ title: NAME_LABEL, field: 'name' },
		{ title: FROM_LABEL, field: 'from', type: 'date' },
		{ title: TO_LABEL, field: 'to', type: 'date' },
		{ title: EXTENT_LABEL, field: 'extent' },
	];
};

export interface IOwnProps {
	id: number;
}


export interface IDispatchProps {}

export interface IStateProps {
	data: I_ROW[];
}

export type IProps = IStateProps;

export function EmployeeDetailProjectsAssignmentTable(props: IProps) {
	const { data } = props;
	const columns = getColumns();

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			options={{
				search: false,
				pageSize: 2,
				pageSizeOptions: [2, 5],
			}}
			components={{
				Container: props => (
					<div {...props}></div>
				),
			}}
		/>
	);
}

export default connect(
(state: IState, props: IOwnProps): IStateProps => {
	const { id } = props;
	const assignments = Object.values(state.projects.data)
		.reduce<ProjectAssignment[]>((prev, curr) => ([...prev, ...curr.projectAssignments]), []);
	const employeesMap = state.employees.data.reduce<{[id: number]: EmployeeAttributes | undefined}>((prev, curr) => ({ ...prev, [curr.id!]: curr }), {});
	const employee = employeesMap[id];
	const assignmentsMapByContractId = assignments
		.reduce<{[id: number]: ProjectAssignment[]}>((prev, curr) => {
			const currContractId = curr.contractId;
			const toReturn = {...prev};
			if (toReturn[currContractId]) {
				toReturn[currContractId].push(curr);
			} else {
				toReturn[currContractId] = [curr];
			}
			return toReturn;
		}, {});

	const employeeAssignments = employee?.contracts
		?.reduce<ProjectAssignment[]>((prev, curr) => {
			return [
				...prev,
				...(assignmentsMapByContractId[curr.id!]
					? assignmentsMapByContractId[curr.id!]
					: []
				),
				];
		}, []);

	return {
		data: employeeAssignments || [],
	};
},
(dispatch: ThunkDispatch): IDispatchProps => {
	dispatch(EmployeesLoad());
	dispatch(ProjectsLoad());
	return {}
},
)(EmployeeDetailProjectsAssignmentTable)
