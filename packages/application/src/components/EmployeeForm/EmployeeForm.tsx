import React from 'react';
import FormSection from '../FormSection/FormSection';
import FormGroup from '../FormGroup/FormGroup';
import { InputGroup, Button, MenuItem } from '@blueprintjs/core';
import Alert from '@material-ui/lab/Alert';
import { DateInput, IDateFormatProps, TimePicker } from '@blueprintjs/datetime';
import { ProjectAssignment, ProjectAssignmentToCreate } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { ProjectsMap } from '../../Projects/helper';
import { ContractToCreate, Contract } from 'domain-model/dist/model/Contract/ContractAttributes';
import NewEmployeeLocalAttributes from '../../Employees/EmployeeLocalAttributes';
import SimulationAttributes from 'domain-model/dist/model/Simulation/SimulationAttributes';
import { convertTimeToMinutes, convertMinutesToTime, convertIsoToDate } from '../../Date/helper';
import { MultiSelect } from '@blueprintjs/select';

import styles from './EmployeeForm.module.css';
import ProjectsAssignmentsTable, { I_ROW as ProjectsAssignmentsTableRow } from '../ProjectsAssignmentsTable/ProjectsAssignmentsTable';
import ContractsTable, { I_ROW as ContractsTableRow } from '../ContractsTable/ContractsTable';
import { useHistory } from 'react-router-dom';
import { EMPLOYEES_PATH } from '../../Router/routes';

type InputChangeEvent = React.ChangeEvent<HTMLInputElement>;

interface IWeekDay {
	id: number;
	name: string;
}
const WeekDaysSelect = MultiSelect.ofType<IWeekDay>();
const WEEK_DAYS = [
	{ id: 0, name: 'Neděle' },
	{ id: 1, name: 'Pondělí' },
	{ id: 2, name: 'Úterý' },
	{ id: 3, name: 'Středa' },
	{ id: 4, name: 'Čtvrtek' },
	{ id: 5, name: 'Pátek' },
	{ id: 6, name: 'Sobota' },
];

const NEW_EMPLOYEE_SECTION_LABEL = 'Nový zaměstnanec';
const EMPLOYEE_NAME_LABEL = 'Jméno';
const EMPLOYEE_ACADEMIC_TITLE_LABEL = 'Titul';
const EMPLOYEE_DATE_OF_BIRTH_LABEL = 'Datum narození';

const SIMULATION_SECTION_LABEL = 'Simulace';
const SIMULATION_HOURS_BEG_LABEL = 'Začátek pracovní doby';
const SIMULATION_HOURS_LENGTH_LABEL = 'Délka pracovní doby';
const SIMULATION_WEEK_DAYS_LABEL = 'Dny v týdnu';
const SIMULATION_WEEK_DAYS_SELECT_PLACEHOLDER = 'Klikni a vyber dny v týdnu';

const SAVE_BUTTON_LABEL = 'Uložit';

export interface IProps {
	availableProjectsMap: ProjectsMap;
	saveEmployeeLocally: (data: Partial<NewEmployeeLocalAttributes>) => void;
	saveSimulationLocally: (data: Partial<SimulationAttributes>) => void;
	saveContractLocally: (index: number, data: Partial<ContractToCreate>) => void;
	removeContractLocally: (id: number) => void;
	createContractLocally: (data: Partial<ContractToCreate>) => void;
	saveProjectAssignmentLocally: (id: number, data: Partial<ProjectAssignment>) => void;
	removeProjectAssignmentLocally: (id: number) => void;
	createProjectAssignmentLocally: (data: Partial<ProjectAssignmentToCreate>) => void;
	saveRemote: () => void;
	name?: string;
	academicTitle?: string;
	dateOfBird: Date;
	simulation: Partial<SimulationAttributes>;
	contracts: (Partial<Contract> & { id: number })[];
	projectsAssignments: Partial<ProjectAssignment>[]; 
	errorMessages: string[];
	created: boolean;
	resetForm: () => void;
}

export default function EmployeeForm(props: IProps) {
	const { 
		availableProjectsMap,
		projectsAssignments,
		contracts,
		saveEmployeeLocally: saveLocally,
		saveSimulationLocally,
		saveContractLocally,
		saveProjectAssignmentLocally,
		saveRemote,
		simulation,
		createProjectAssignmentLocally,
		createContractLocally,
		removeProjectAssignmentLocally,
		removeContractLocally,
		errorMessages,
		created,
		resetForm,
	} = props;
	const history = useHistory();

	const selectedWeekDays: IWeekDay[] = simulation.weekDaysJSONArray
		? JSON.parse(simulation.weekDaysJSONArray).map((val: number) => WEEK_DAYS[val])
		: [];

	const availableProjects = Object.values(availableProjectsMap);

	const jsDateFormatter: IDateFormatProps = {
		// note that the native implementation of Date functions differs between browsers
		formatDate: date => date.toLocaleDateString(),
		parseDate: str => new Date(str),
		placeholder: "M/D/YYYY",
	};

	const academicTitle = props.academicTitle ? props.academicTitle : '';

	const onNameChange = (event: InputChangeEvent) => saveLocally({name: event.target.value});
	const onTitleChange = (event: InputChangeEvent) => saveLocally({academicTitle: event.target.value});
	const onDateOfBirthChange = (date: Date) => saveLocally({dateOfBird: date.toISOString() as any});

	const workingHoursBeginningMinutesFrom = simulation.workingHoursBeginningMinutesFrom
		? convertMinutesToTime(simulation.workingHoursBeginningMinutesFrom)
		: undefined;
	const workingHoursBeginningMinutesTo = simulation.workingHoursBeginningMinutesTo
		? convertMinutesToTime(simulation.workingHoursBeginningMinutesTo)
		: undefined;
	const workingHoursLengthMinutesFrom = simulation.workingHoursLengthMinutesFrom
		? convertMinutesToTime(simulation.workingHoursLengthMinutesFrom)
		: undefined;
	const workingHoursLengthMinutesTo = simulation.workingHoursLengthMinutesTo
		? convertMinutesToTime(simulation.workingHoursLengthMinutesTo)
		: undefined;
	const onSimulationHoursBegFromChange = (time: Date) => saveSimulationLocally({ workingHoursBeginningMinutesFrom: convertTimeToMinutes(time) });
	const onSimulationHoursBegToChange = (time: Date) => saveSimulationLocally({  workingHoursBeginningMinutesTo: convertTimeToMinutes(time) });
	const onSimulationHoursLengthFromChange = (time: Date) => saveSimulationLocally({ workingHoursLengthMinutesFrom: convertTimeToMinutes(time) });
	const onSimulationHoursLengthToChange = (time: Date) => saveSimulationLocally({ workingHoursLengthMinutesTo: convertTimeToMinutes(time) });
	const onSimulationWeekDaysChange = (day: IWeekDay) => {
		const itemIndex = selectedWeekDays.indexOf(day);
		if (itemIndex > -1) {
			selectedWeekDays.splice(itemIndex, 1);
			saveSimulationLocally({
				weekDaysJSONArray: JSON.stringify(selectedWeekDays.map((day) => day.id)),
			});
		} else {
			saveSimulationLocally({
				weekDaysJSONArray: JSON.stringify([...selectedWeekDays, day].map((day) => day.id)),
			});
		}
	};
	const onSimulationWeekDaysRemove = (_: string, dayIndex: number) => {
		selectedWeekDays.splice(dayIndex, 1);
		saveSimulationLocally({
			weekDaysJSONArray: JSON.stringify(selectedWeekDays.map((day) => day.id)),
		});
	};

	const contractsTableData: ContractsTableRow[] = contracts.map((contract, index): ContractsTableRow => {
		return {
			id: contract.id,
			type: contract.type as string,
			extent: contract.extent?.toString(),
			from: convertIsoToDate(contract.from),
			to: convertIsoToDate(contract.to),
		};
	});

	const projectsAssignmentsTableData: ProjectsAssignmentsTableRow[] = projectsAssignments.map((assignment): ProjectsAssignmentsTableRow => {
		return {
			id: assignment.id,
			contractId: assignment.contractId?.toString(),
			projectId: assignment.projectId?.toString(),
			extent: assignment.extent,
			from: convertIsoToDate(assignment.from),
			to: convertIsoToDate(assignment.to),
		};
	});

	const onEmployeeSaveBtnCreate = () => {
		saveRemote();
	}

	if (created) {
		resetForm();
		history.push(EMPLOYEES_PATH);
	}

	return (
		<div>
			<div>
				<FormSection name={NEW_EMPLOYEE_SECTION_LABEL}>
					<FormGroup name={EMPLOYEE_NAME_LABEL}><InputGroup type="text" onChange={onNameChange} value={props.name} /></FormGroup>
					<FormGroup name={EMPLOYEE_ACADEMIC_TITLE_LABEL}><InputGroup type="text" onChange={onTitleChange} value={academicTitle} /></FormGroup>
					<FormGroup name={EMPLOYEE_DATE_OF_BIRTH_LABEL}>
						<DateInput {...jsDateFormatter} onChange={onDateOfBirthChange} value={props.dateOfBird}
							minDate={new Date('1900')}	
						/>
					</FormGroup>
				</FormSection>
				<FormSection name={SIMULATION_SECTION_LABEL}>
					<FormGroup name={SIMULATION_HOURS_BEG_LABEL}>
						<TimePicker onChange={onSimulationHoursBegFromChange} value={workingHoursBeginningMinutesFrom}/>
						<TimePicker onChange={onSimulationHoursBegToChange} value={workingHoursBeginningMinutesTo}/>
					</FormGroup>
					<FormGroup name={SIMULATION_HOURS_LENGTH_LABEL}>
						<TimePicker onChange={onSimulationHoursLengthFromChange} value={workingHoursLengthMinutesFrom}/>
						<TimePicker onChange={onSimulationHoursLengthToChange} value={workingHoursLengthMinutesTo}/>
					</FormGroup>
					<FormGroup name={SIMULATION_WEEK_DAYS_LABEL}>
						<WeekDaysSelect
							items={WEEK_DAYS}
							selectedItems={selectedWeekDays}
							tagRenderer={(item) => item.name}
							itemRenderer={(item, itemProps) => <MenuItem key={item.id+item.name} text={item.name} onClick={itemProps.handleClick} shouldDismissPopover={false}/>}
							onItemSelect={onSimulationWeekDaysChange}
							tagInputProps={{onRemove: onSimulationWeekDaysRemove}}
							placeholder={SIMULATION_WEEK_DAYS_SELECT_PLACEHOLDER}
						/>
					</FormGroup>
				</FormSection>
			</div>
			<div>
				<ContractsTable
					data={contractsTableData}
					update={saveContractLocally}
					create={createContractLocally}
					remove={removeContractLocally}
				/>
			</div>
			<br/>
			<div>
				<ProjectsAssignmentsTable
					contracts={contracts}
					projects={availableProjects}
					data={projectsAssignmentsTableData}
					update={saveProjectAssignmentLocally}
					create={createProjectAssignmentLocally}
					remove={removeProjectAssignmentLocally}
				/>				
			</div>
			<div>
				{errorMessages.map((errorMessage, index) => <Alert key={index} severity="error">{errorMessage}</Alert>)}
			</div>
			<div>
				<Button intent={'primary'} onClick={onEmployeeSaveBtnCreate}>{SAVE_BUTTON_LABEL}</Button>
			</div>
		</div>
	);
}
