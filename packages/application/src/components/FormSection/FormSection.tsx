import React from 'react';
import { Card, Elevation } from '@blueprintjs/core';

import styles from './FormSection.module.css';

export interface IProps {
	name: string;
	children: React.ReactNode;
}

export default function FormSection(props: IProps) {
	const { name, children } = props;

	return (
		<div className={styles.container}>
			<h5>{name}</h5>
			<Card elevation={Elevation.ONE}>
				{children}
			</Card>
		</div>
	);
}
