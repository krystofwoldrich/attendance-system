import React, { useState } from 'react';
import { DateRangePicker as DateRangePickerBlueJS, DateRange } from "@blueprintjs/datetime";

export interface IProps {
	onChange?: (selectedDays: DateRange) => void;
}

export function DateRangePicker(props: IProps) {
	const { onChange } = props;

	const [startDate, setStartDate] = useState<Date | undefined>(undefined);
	const [endDate, setEndDate] = useState<Date | undefined>(undefined);

	const value: DateRange = [startDate, endDate];
	const handleDateChange = (selectedDays: DateRange) => {
		const [selectedStartDate, selectedEndDate] = selectedDays;
		setStartDate(selectedStartDate);
		setEndDate(selectedEndDate);

		if (onChange && selectedStartDate && selectedEndDate) {
			onChange(selectedDays);
		}
	};

	return (
		<DateRangePickerBlueJS
			value={value}
			onChange={handleDateChange}
		/>
	);
}
