import React from 'react';
import { connect } from 'react-redux';
import { IState } from '../../reducer';
import { Card, TextField } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { ThunkDispatch } from '../../store';
import { EmployeesLoad } from '../../Employees/employeesThunk';
import DateFnsUtils from '@date-io/date-fns';

import styles from './EmployeeDetailContainer.module.css';

const NAME_LABEL = 'Jméno zaměstnance';
const ACADEMIC_TITLE_LABEL = 'Akademický titul';
const DATE_OF_BIRTH_LABEL = 'Datum narození';

export interface IOwnProps {
	id: number;
}

export interface IStateProps {
	name?: string;
	academicTitle?: string | null;
	dateOfBirth?: Date;
}

export type IProps = IStateProps;

export function EmployeeDetailContainer(props: IProps) {
	const { name, academicTitle, dateOfBirth } = props;

	return (
		<div className={styles.container}>
			<MuiPickersUtilsProvider utils={DateFnsUtils}>
				<div>
					<TextField
						disabled
						label={NAME_LABEL}
						variant='filled'
						size='small'
						value={name || 'Loading...'}
					/>
				</div>
				<br/>
				<div>
					<TextField
						disabled
						label={ACADEMIC_TITLE_LABEL}
						variant='filled'
						size='small'
						value={academicTitle || 'Loading...'}
					/>
				</div>
				<KeyboardDatePicker
					disabled
					disableToolbar
					variant='inline'
					inputVariant='filled'
					size='small'
					format='MM/dd/yyyy'
					margin='none'
					label={DATE_OF_BIRTH_LABEL}
					value={dateOfBirth}
					onChange={() => {}}
					KeyboardButtonProps={{
						'aria-label': 'change date',
					}}
				/>
			</MuiPickersUtilsProvider>
		</div>
	);
}

export default connect(
	(state: IState, props: IOwnProps): IStateProps => {
		const { id } = props;
		const employeesMap = state.employees.data.reduce<{[id: number]: EmployeeAttributes | undefined}>((prev, curr) => ({ ...prev, [curr.id!]: curr }), {});

		return {
			name: employeesMap[id]?.name,
			academicTitle: employeesMap[id]?.academicTitle,
			dateOfBirth: employeesMap[id]?.dateOfBird,
		};
	},
	(dispatch: ThunkDispatch): {} => {
		dispatch(EmployeesLoad());
		return {};
	},
)(EmployeeDetailContainer);
