import { ThunkAction } from '../store';
import { RecordsLoadSuccess, RecordsLoadRequest, RecordsLoadError, NewRecordCreateSuccess, RecordUpdateSuccess, RecordDeleteSuccess } from './recordsActions';
import { Record, RecordToCreate, RecordToUpdate } from 'domain-model/dist/model/Record/RecordAttributes';
import { doGet } from '../Api/doRequest';
import createRecord from '../Api/Records/createRecord';
import updateRecord from '../Api/Records/updateRecord';
import deleteRecord from '../Api/Records/deleteRecord';

export const RecordsLoad = (): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const { api } = state;
	const apiBaseUrl = api.baseUrl;
	const resource = 'records';
	const url = `${apiBaseUrl}/${resource}`;

	try {
		dispatch({
			type: RecordsLoadRequest,
		} as RecordsLoadRequest);
		const records = await doGet<Record[]>(url);
		dispatch<RecordsLoadSuccess>({
			type: RecordsLoadSuccess,
			response: records,
		});
	} catch (error) {
		dispatch({
			type: RecordsLoadError,
			message: error.message,
		} as RecordsLoadError)
	}
};

export const NewRecordCreate = (data: RecordToCreate): ThunkAction => {
	return async (dispatch, getState) => {
		const state = getState();
		const baseUrl = state.api.baseUrl;

		try {
			const record = await createRecord(
				baseUrl,
				data,
			);
			dispatch<NewRecordCreateSuccess>({
				type: NewRecordCreateSuccess,
				data: record,
			});
		} catch (error) {
			//dispatch error;
		}
	};
}

export const RecordUpdate = (data: RecordToUpdate): ThunkAction => {
	return async (dispatch, getState) => {
		const state = getState();
		const baseUrl = state.api.baseUrl;

		try {
			const record = await updateRecord(
				baseUrl,
				data,
			);
			dispatch<RecordUpdateSuccess>({
				type: RecordUpdateSuccess,
				data: record,
			});
		} catch (error) {
			//dispatch error;
		}
	};
}

export const RecordDelete = (id: number): ThunkAction => {
	return async (dispatch, getState) => {
		const state = getState();
		const baseUrl = state.api.baseUrl;

		try {
			const record = await deleteRecord(
				baseUrl,
				id,
			);
			dispatch<RecordDeleteSuccess>({
				type: RecordDeleteSuccess,
				data: record,
			});
		} catch (error) {
			//dispatch error;
		}
	};
}
