import { Record, RecordToCreate } from 'domain-model/dist/model/Record/RecordAttributes';

export const RecordsLoadRequest = 'Records.Load.Request';
export interface RecordsLoadRequest {
	type: typeof RecordsLoadRequest;
}

export const RecordsLoadSuccess = 'Records.Load.Success';
export interface RecordsLoadSuccess {
	type: typeof RecordsLoadSuccess;
	response: Record[];
}

export const RecordsLoadError = 'Records.Load.Error';
export interface RecordsLoadError {
	type: typeof RecordsLoadError;
	message: string;
}

export const NewRecordCreateSuccess = 'New.Record.Create.Success';
export interface NewRecordCreateSuccess {
	type: typeof NewRecordCreateSuccess;
	data: Record;
}

export const RecordUpdateSuccess = 'Record.Update.Success';
export interface RecordUpdateSuccess {
	type: typeof RecordUpdateSuccess;
	data: Record;
}

export const RecordDeleteSuccess = 'Record.Delete.Success';
export interface RecordDeleteSuccess {
	type: typeof RecordDeleteSuccess;
	data: Record;
}

type RecordsActions = RecordsLoadRequest
	| RecordsLoadSuccess
	| RecordsLoadError
	| NewRecordCreateSuccess
	| RecordUpdateSuccess
	| RecordDeleteSuccess;

export default RecordsActions;
