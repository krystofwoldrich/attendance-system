import React from 'react';
import styles from './Login.module.css';
import LoginForm from '../components/LoginForm/LoginForm';

export interface IProps {}

export default function Login(_props: IProps) {
	const containerClassName = styles.container;
	const loginFormContainerClassName = styles.loginFormContainer

	return (
		<div className={containerClassName}>
			<div className={loginFormContainerClassName}>
				<LoginForm />
			</div>
		</div>
	);
}
