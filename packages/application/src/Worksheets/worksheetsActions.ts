
export interface WorksheetCreateAction {
	employeeId: number;
	projectId: number;
	from: Date;
	to: Date;
	requestedAt: Date;
}

export const WorksheetCreateRequest = 'Worksheet.Create.Request';
export interface WorksheetCreateRequest extends WorksheetCreateAction {
	type: typeof WorksheetCreateRequest;
}

export const WorksheetCreateSuccess = 'Worksheet.Create.Success';
export interface WorksheetCreateSuccess extends WorksheetCreateAction {
	type: typeof WorksheetCreateSuccess;
}

export const WorksheetCreateCancel = 'Worksheet.Create.Cancel';
export interface WorksheetCreateCancel extends WorksheetCreateAction {
	type: typeof WorksheetCreateCancel;
}

export const WorksheetCreateError = 'Worksheets.Create.Error';
export interface WorksheetCreateError extends WorksheetCreateAction {
	type: typeof WorksheetCreateError;
	message: string;
}

type WorksheetsActions = WorksheetCreateRequest |
	WorksheetCreateSuccess |
	WorksheetCreateError |
	WorksheetCreateCancel;

export default WorksheetsActions;
