
export function convertTimeToMinutes(time: Date): number {
	return (time.getHours() * 60) + time.getMinutes();
}

export function convertMinutesToTime(minutes: number): Date {
	return new Date(0, 0, 0, 0, minutes);
}

export function convertIsoToDate(iso: string | undefined): Date | undefined {
	return iso ? new Date(iso) : undefined;
}
