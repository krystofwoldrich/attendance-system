import { Location } from 'history';

export const HistoryLocationChanged = 'History.Location.Changed';
export interface HistoryLocationChanged {
	type: typeof HistoryLocationChanged,
	location: Location,
}
