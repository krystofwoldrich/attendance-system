import { rejects } from 'assert';

export default function wait(milliseconds: number = 1) {
	return new Promise((resolve) => {
		setTimeout(resolve, milliseconds);
	});
}
