
export function deepJSONCopy<T>(serializable: T): T {
	return JSON.parse(JSON.stringify(serializable));
}
