import { ProjectsMap } from './helper';
import LOAD_STATE from '../LoadState/LoadState';
import ProjectsActions, {
	ProjectsLoadRequest,
	ProjectsLoadError,
	ProjectsLoadSuccess,
} from './projectsActions';

export interface IProjectsState {
	data: ProjectsMap;
	state: LOAD_STATE;
	error: string | undefined;
}

const initState: IProjectsState = {
	data: {},
	state: LOAD_STATE.INITIAL,
	error: undefined,
}

export default function (state: IProjectsState = initState, action: ProjectsActions): IProjectsState {

	switch (action.type) {
		case ProjectsLoadRequest:
			return {
				...state,
				state: LOAD_STATE.LOADING,
			};

		case ProjectsLoadSuccess:
			const projectsMap = action.response.reduce<ProjectsMap>((prev, current) => {
				if (current.id) {
					prev[current.id] = current;
				}
				return prev;
			}, {});
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				data: projectsMap,
			};

		case ProjectsLoadError:
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				error: action.message,
			};
	
		default:
			return state;
	}
}