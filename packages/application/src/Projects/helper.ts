import { Project } from 'domain-model/dist/model/Project/ProjectAttributes';
import { ProjectAssignment } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { ContractAttributes } from 'domain-model/dist/model/Contract/ContractAttributes';

export type ProjectsMap = {[key: number]: Project};
export type AssignmentsMapByContractIds = {[contractId: number]: ProjectAssignment[]};

export function getProjectAssignmentsMapByContractIds(assignments: ProjectAssignment[]) {
	const assignmentsMapByContractId = assignments.reduce<{ [id: number]: ProjectAssignment[] }>((prev, curr) => {
		const toReturn = { ...prev };
		if (toReturn[curr.contractId]) {
			toReturn[curr.contractId].push(curr);
		} else {
			toReturn[curr.contractId] = [curr];
		}
		return toReturn;
	}, {});

	return assignmentsMapByContractId;
}

export function getEmployeeAssignments(assignments: AssignmentsMapByContractIds, employeeContracts: ContractAttributes[]) {
	const employeeAssignments = employeeContracts
		?.reduce<ProjectAssignment[]>((prev, curr) => {
			return [
				...prev,
				...(assignments[curr.id!]
					? assignments[curr.id!]
					: []),
			];
	}, []);

	return employeeAssignments;
}

export function getEmployeeProjects(assignments: ProjectAssignment[], projects: ProjectsMap) {
	const employeeProjects = assignments.reduce<ProjectsMap>(
		(map, assignment) => {
			return ({ ...map, [projects[assignment.projectId].id]: projects[assignment.projectId]});
		}, 
		{},
	);

	return employeeProjects;
}

export function getProjectsAssignments(projects: Project[]) {
	const assignments = projects.reduce<ProjectAssignment[]>((prev, curr) => ([...prev, ...curr.projectAssignments]), []);

	return assignments;
}