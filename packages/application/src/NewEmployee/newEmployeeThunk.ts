import { ThunkAction } from '../store';
import { NewEmployeeCreate, NewEmployeeCreateValidationError } from './newEmployeeActions';
import createEmployee from '../Api/Employees/createEmployee';
import createSimulation from '../Api/Employees/Simulation/createSimulation';
import createContract from '../Api/Employees/Contract/createContract';
import createProjectAssignment from '../Api/Projects/createProjectAssignment';
import { Contract } from 'domain-model/dist/model/Contract/ContractAttributes';
import wait from '../System/wait';
import { EmployeeToCreate } from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { SimulationToCreate } from 'domain-model/dist/model/Simulation/SimulationAttributes';

export const NewEmployeeSave = (): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const baseUrl = state.api.baseUrl;
	const employeeFromState = state.newEmployee.employee;
	const simulationFromState = state.newEmployee.simulation;
	const contractsFromState = Object.values(state.newEmployee.contracts);
	const projectsAssignmentsFromState = state.newEmployee.projectsAssignments;
	const errors: string[] = [];

	if (!employeeFromState.name) {
		errors.push('Employee\'s name is missing. Please, fill in the name input.')
	}
	if (!employeeFromState.academicTitle) {
		errors.push('Employee\'s academic title is missing. Please, fill in the title input.')
	}
	if (!employeeFromState.dateOfBird) {
		errors.push('Employee\' date of birth is missing. Please, fill in the date of birth input.')
	}

	const employeeToCreate: EmployeeToCreate = {
		name: employeeFromState.name!,
		academicTitle: employeeFromState.academicTitle!,
		dateOfBird: employeeFromState.dateOfBird!,
	};

	if (!simulationFromState.workingHoursBeginningMinutesFrom) {
		errors.push('Employee\' working hours beginning from is missing. Please, fill in the working hours beginning from input.')
	}
	if (!simulationFromState.workingHoursBeginningMinutesTo) {
		errors.push('Employee\' working hours beginning to is missing. Please, fill in the working hours beginning to input.')
	}
	if (!simulationFromState.workingHoursLengthMinutesFrom) {
		errors.push('Employee\' working hours length from is missing. Please, fill in the working hours beginning from input.')
	}
	if (!simulationFromState.workingHoursLengthMinutesTo) {
		errors.push('Employee\' working hours length to is missing. Please, fill in the working hours beginning to input.')
	}
	if (!simulationFromState.weekDaysJSONArray) {
		errors.push('Employee\' working week day are missing. Please, contact support.')
	}

	const simulationToCreate: SimulationToCreate = {
		employeeId: -1,
		workingHoursBeginningMinutesFrom: simulationFromState.workingHoursBeginningMinutesFrom!,
		workingHoursBeginningMinutesTo: simulationFromState.workingHoursBeginningMinutesTo!,
		workingHoursLengthMinutesFrom: simulationFromState.workingHoursLengthMinutesFrom!,
		workingHoursLengthMinutesTo: simulationFromState.workingHoursLengthMinutesTo!,
		weekDaysJSONArray: simulationFromState.weekDaysJSONArray!,
	}

	//TODO: check contracts

	const contractsToCreate = contractsFromState.map((contract) => ({
		localId: contract.id,
		type: contract.type!,
		extent: contract.extent!,
		from: contract.from!,
		to: contract.to!,
	}));

	//TODO: check projects assignments

	const projectsAssignmentsToCreate = projectsAssignmentsFromState.map((assignment) => ({
		contractId: undefined,
		contractLocalId: assignment.contractId!,
		projectId: assignment.projectId!,
		position: assignment.position!,
		from: assignment.from!,
		to: assignment.to!,
		extent: assignment.extent!,
	}));

	if (errors.length > 0) {
		dispatch<NewEmployeeCreateValidationError>({
			type: NewEmployeeCreateValidationError,
			errorsMessages: errors,
		});
		return;
	}

	try {
		const employee = await createEmployee(baseUrl, employeeToCreate);
		const simulation = await createSimulation(
			baseUrl,
			{
				...simulationToCreate,
				employeeId: employee.id,
			},
		);
		const contractsCreators = contractsToCreate.map(async (contract) => {
			const contractCreated = await createContract(
				baseUrl,
				{
					...contract,
					employeeId: employee.id,
				},
			);
			return {
				...contractCreated,
				localId: contract.localId,
			};
		})
		const contracts = await Promise.all(contractsCreators);
		const contractLocalMap = contracts.reduce<{ [id: number]: Contract }>((prev, curr) => {
			return {
				...prev,
				[curr.localId]: curr,
			};
		}, {});
		await wait(10);
		const projectsAssignmentsCreators = projectsAssignmentsToCreate.map((assignment) => {
			return createProjectAssignment(
				baseUrl,
				{
					...assignment,
					contractId: contractLocalMap[assignment.contractLocalId].id,
				},
			);
		});
		await Promise.all(projectsAssignmentsCreators);
		employee.simulationData = simulation;
		employee.contracts = contracts;
		dispatch<NewEmployeeCreate>({
			type: NewEmployeeCreate,
			response: employee,
		});
	} catch (error) {
		//TODO: Error action
	}
};
