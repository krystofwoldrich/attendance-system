import NewEmployeeActions, {
	NewEmployeeDetailChange, NewEmployeeSimulationChange, NewEmployeeContractChange, NewEmployeeProjectAssignmentChange, NewEmployeeProjectAssignmentCreate, NewEmployeeContractCreate, NewEmployeeContractRemove, NewEmployeeCreate, NewEmployeeCreateValidationError, NewEmployeeCreateResetForm,
} from './newEmployeeActions';
import EmployeeLocalAttributes from '../Employees/EmployeeLocalAttributes';
import SimulationAttributes from 'domain-model/dist/model/Simulation/SimulationAttributes';
import { ContractToCreate, Contract } from 'domain-model/dist/model/Contract/ContractAttributes';
import { ProjectAssignment } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import ContractLocalIdGenerator from '../Contract/helper';

const contractLocalIdGenerator = ContractLocalIdGenerator();

export interface INewEmployeeState {
	employee: Partial<Pick<EmployeeLocalAttributes, 'name' | 'academicTitle' | 'dateOfBird'>>;
	simulation: Partial<SimulationAttributes>;
	contracts: { [id: number]: (Partial<Contract> & { id: number }) };
	projectsAssignments: Partial<ProjectAssignment>[]; 
	errorMessages: string[],
	created: boolean,
}

const initState: INewEmployeeState = {
	employee: {},
	simulation: {},
	contracts: [],
	projectsAssignments: [],
	errorMessages: [],
	created: false,
};

export default function (state: INewEmployeeState = initState, action: NewEmployeeActions): INewEmployeeState {

	switch (action.type) {
		case NewEmployeeCreate:
			contractLocalIdGenerator.reset();
			return {
				...initState,
				created: true,
			};

		case NewEmployeeCreateResetForm:
			contractLocalIdGenerator.reset();
			return initState;

		case NewEmployeeDetailChange:
			return {
				...state,
				employee: {
					...state.employee,
					...action.employee,
				},
			};
	
		case NewEmployeeSimulationChange:
			return {
				...state,
				simulation: {
					...state.simulation,
					...action.simulation,
				},
			};
	
		case NewEmployeeContractCreate:
			const contractId = contractLocalIdGenerator.generate();
			return {
				...state,
				contracts: {
					...state.contracts,
					[contractId]: {
						...action.contract,
						id: contractId,
					},
				},
			};
	
		case NewEmployeeContractChange:
			return {
				...state,
				contracts: {
					...state.contracts,
					[action.id]: {
						...state.contracts[action.id],
						...action.contract,
					}
				},
			};
	
		case NewEmployeeContractRemove:
			const contractsRemoved = { ...state.contracts };
			delete contractsRemoved[action.id];
			return {
				...state,
				contracts: contractsRemoved,
			};

		case NewEmployeeProjectAssignmentCreate:
			return {
				...state,
				projectsAssignments: [
					...state.projectsAssignments,
					action.projectAssignment,
				],
			};

		case NewEmployeeProjectAssignmentChange:
			const projectsAssignments = [...state.projectsAssignments];
			projectsAssignments[action.index] = {
				...projectsAssignments[action.index],
				...action.projectAssignment,
			};
			return {
				...state,
				projectsAssignments,
			};
	
		case NewEmployeeCreateValidationError:
			return {
				...state,
				errorMessages: action.errorsMessages,
			}

		default:
			return state;
	}
}