import { ThunkAction } from '../store';
import { EmployeesLoadSuccess, EmployeesLoadRequest, EmployeesLoadError, EmployeeSimulationRunRequest, EmployeeSimulationRunSuccess, EmployeeSimulationRunError } from './employeesActions';
import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import runSimulation from '../Api/Employees/Simulation/runSimulation';
import { RecordsLoad } from '../Records/recordsThunk';

export const EmployeesLoad = (): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const { api } = state;
	const apiBaseUrl = api.baseUrl;
	const resource = 'employees';
	const url = `${apiBaseUrl}/${resource}`;

	try {
		dispatch({
			type: EmployeesLoadRequest,
		} as EmployeesLoadRequest);
		const response = await fetch(url, { method: 'GET' });
		const employees = (await response.json()) as EmployeeAttributes[];
		dispatch({
			type: EmployeesLoadSuccess,
			response: employees,
		} as EmployeesLoadSuccess);
	} catch (error) {
		dispatch({
			type: EmployeesLoadError,
			message: error.message,
		} as EmployeesLoadError)
	}
};

export const EmployeeSimulationRun = (
	employeeId: number,
    projectAssignmentId: number,
    from: Date,
    to: Date,
): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const { api } = state;
	const apiBaseUrl = api.baseUrl;

	try {
		dispatch<EmployeeSimulationRunRequest>({
			type: EmployeeSimulationRunRequest,
		});
		await runSimulation(apiBaseUrl, {
			employeeId,
			projectAssignmentId,
			from,
			to,
		});
		dispatch(RecordsLoad());
		dispatch<EmployeeSimulationRunSuccess>({
			type: EmployeeSimulationRunSuccess,
		});
	} catch (error) {
		dispatch<EmployeeSimulationRunError>({
			type: EmployeeSimulationRunError,
			message: error.message,
		});
	}
};
