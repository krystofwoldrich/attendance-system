import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';

export type EmployeesMap = {[id: number]: EmployeeAttributes | undefined};

export function getEmployeesMap(employees: EmployeeAttributes[]) {
	const employeesMap = employees.reduce<EmployeesMap>((prev, curr) => ({ ...prev, [curr.id!]: curr }), {});

	return employeesMap;
}
