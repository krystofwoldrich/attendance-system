import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import EmployeeDetailProjectsAssignmentTable from '../components/EmployeeDetailProjectsAssignmentTable/EmployeeDetailProjectsAssignmentTable';
import EmployeeDetailContainer from '../components/EmployeeDetailContainer/EmployeeDetailContainer';
import EmployeeDetailRecordsTable from '../components/EmployeeDetailRecordsTable/EmployeeDetailRecordsTable';
import EmployeeProjectsSelect from '../components/EmployeeProjectsSelect/EmployeeProjectsSelect';
import EmployeeDetailActionBar from '../components/EmployeeDetailActionBar/EmployeeDetailActionBar';

export default function EmployeeDetail() {
	const [ selectedProjectId, setSelectedProjectId ] = useState<number | undefined>();
	const { id } = useParams();

	const employeeId = id ? parseInt(id) : NaN;

	if (employeeId === NaN) {
		return (
			<div></div>
		);
	} else {
		return (
			<div>
				<EmployeeDetailContainer id={employeeId}/>
				<EmployeeDetailProjectsAssignmentTable id={employeeId}/>
				<br/>
				<EmployeeProjectsSelect
					employeeId={employeeId}
					onSelect={setSelectedProjectId}
				/>
				<br/>
				{
					selectedProjectId
						? <EmployeeDetailRecordsTable
							employeeId={employeeId}
							projectId={selectedProjectId}
							showFrom={null}
							showTo={null}
						/>
						: null
				}
				<br/>
				<EmployeeDetailActionBar
					employeeId={employeeId}
					projectId={selectedProjectId}
				/>
			</div>
		);
	}
}
