import EmployeesAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import LOAD_STATE from '../LoadState/LoadState';
import EmployeesActions, {
	EmployeesLoadRequest,
	EmployeesLoadError,
	EmployeesLoadSuccess,
} from './employeesActions';

export interface IEmployeesState {
	data: EmployeesAttributes[];
	state: LOAD_STATE;
	error: string | undefined;
}

const initState: IEmployeesState = {
	data: [],
	state: LOAD_STATE.INITIAL,
	error: undefined,
}

export default function (state: IEmployeesState = initState, action: EmployeesActions): IEmployeesState {

	switch (action.type) {
		case EmployeesLoadRequest:
			return {
				...state,
				state: LOAD_STATE.LOADING,
			};

		case EmployeesLoadSuccess:
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				data: action.response,
			};

		case EmployeesLoadError:
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				error: action.message,
			};
	
		default:
			return state;
	}
}