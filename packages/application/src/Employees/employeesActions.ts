import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';

export const EmployeesLoadRequest = 'Employees.Load.Request';
export interface EmployeesLoadRequest {
	type: typeof EmployeesLoadRequest;
}

export const EmployeesLoadSuccess = 'Employees.Load.Success';
export interface EmployeesLoadSuccess {
	type: typeof EmployeesLoadSuccess;
	response: EmployeeAttributes[];
}

export const EmployeesLoadError = 'Employees.Load.Error';
export interface EmployeesLoadError {
	type: typeof EmployeesLoadError;
	message: string;
}

export const EmployeeSimulationRunRequest = 'Employee.Simulation.Run.Run.Request';
export interface EmployeeSimulationRunRequest {
	type: typeof EmployeeSimulationRunRequest;
}

export const EmployeeSimulationRunSuccess = 'Employee.Simulation.Run.Success';
export interface EmployeeSimulationRunSuccess {
	type: typeof EmployeeSimulationRunSuccess;
}

export const EmployeeSimulationRunError = 'Employee.Simulation.Run.Error';
export interface EmployeeSimulationRunError {
	type: typeof EmployeeSimulationRunError;
	message: string;
}

type EmployeesActions = EmployeesLoadRequest
	| EmployeesLoadSuccess
	| EmployeesLoadError
	| EmployeeSimulationRunRequest
	| EmployeeSimulationRunSuccess
	| EmployeeSimulationRunError;

export default EmployeesActions;
