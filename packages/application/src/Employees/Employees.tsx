import React from 'react';
import styles from './Employees.module.css';
import EmployeesTable from '../components/EmployeesTable/EmployeesTable';
import ActionBar from '../components/ActionBar/ActionBar';
import { connect } from 'react-redux';
import { IState } from '../reducer';
import { EmployeesLoad } from './employeesThunk';
import LOAD_STATE from '../LoadState/LoadState';
import { ThunkDispatch } from '../store';
import { Link } from 'react-router-dom';

const ADD_NEW_EMPLOYEE_BTN_LABEL = 'Add new employee';

export interface IOwnProps {}

export interface IStateProps {
	dataState: LOAD_STATE;
}

export interface IDispatchProps {
}

export type IProps = IOwnProps & IDispatchProps & IStateProps;

export function Employees(props: IProps) {
	return (
		<div className={styles.container}>
			<ActionBar>
				<Link className={'bp3-button bp3-intent-primary'} to={'/employee/new'}>{ADD_NEW_EMPLOYEE_BTN_LABEL}</Link>
			</ActionBar>
			<div className={styles.tableContainer}>
				<EmployeesTable />
			</div>
		</div>
	);
}

export default connect(
	(state: IState, ownProps: IOwnProps): IStateProps => ({
		dataState: state.employees.state,
	}),
	(dispatch: ThunkDispatch) => {
		dispatch(EmployeesLoad());
		return {};
	},
)(Employees);
