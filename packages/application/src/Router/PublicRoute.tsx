import React from 'react';
import { RouteProps, Route } from 'react-router-dom';

export interface IProps {
	children: React.ReactNode
}

export default function PublicRoute(props: IProps & RouteProps) {
	const { children } = props;

	const wrapped = (
		<React.Fragment>
			{children}
		</React.Fragment>
	);

	return (
		<Route {...props} children={undefined} render={() => wrapped}/>
	);
}
