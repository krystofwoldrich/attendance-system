import {Sequelize} from 'sequelize-typescript';
import SimulationModel from './model/Simulation/SimulationModel';
import EmployeeModel from './model/Employee/EmployeeModel';
import ContractModel from './model/Contract/ContractModel';
import ProjectModel from './model/Project/ProjectModel';
import ProjectAssignmentModel from './model/Project/ProjectAssignmentModel';
import RecordModel from './model/Record/RecordModel';

const DATABASE_NAME = 'main';

export const connect = async (
	url: string,
	logging: boolean = true,
	_username?: string,
	_password?: string,
) => {
	const sequelize = new Sequelize({
		dialect: 'sqlite',
		database: DATABASE_NAME,
		storage: url,
		models: [
			EmployeeModel,
			SimulationModel,
			ContractModel,
			ProjectModel,
			ProjectAssignmentModel,
			RecordModel,
		],
		logging,
	});
	
	//TODO: Change for production env
	await sequelize.sync({ force: true })

	return sequelize;
}
