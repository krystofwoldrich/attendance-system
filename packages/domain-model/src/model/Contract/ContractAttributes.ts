import { ContractType } from './ContractType';
import { ProjectAssignmentAttributes } from '../Project/ProjectAssignmentAttributes';

export interface ContractToCreate {
	employeeId: number;
	type: ContractType;
	extent: number;
	from: string;
	to: string;
}

export interface Contract extends ContractToCreate {
	id:number;
	createdAt: string;
	updatedAt: string;
}

export interface ContractAttributes {
	id?:number;
	employeeId?: number;
	projectAssignments?: ProjectAssignmentAttributes[] | null; 
	type: ContractType;
	extent: number;
	from: Date;
	to: Date;
	createdAt?: Date;
	updatedAt?: Date;
}