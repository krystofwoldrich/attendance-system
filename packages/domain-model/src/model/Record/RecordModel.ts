import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { RecordAttributes } from './RecordAttributes';
import RecordType from './RecordType';
import ProjectModel from '../Project/ProjectModel';
import EmployeeModel from '../Employee/EmployeeModel';

@Table({
	tableName: 'record',
})
export default class RecordModel extends Model<RecordModel> implements RecordAttributes {
	
	@ForeignKey(() => ProjectModel)
	@Column
	projectId?: number;
	
	@ForeignKey(() => EmployeeModel)
	@Column
	employeeId?: number;
	
	@Column
	description?: string;
	
	@Column
	type: RecordType;
	
	@Column
	from: Date;
	
	@Column
	to: Date;
}
