
type RecordType = InclusiveRecordType | ExclusiveRecordType;

type InclusiveRecordType = 'inclusiveGeneral'
	| 'office'
	| 'businessTrip'
	| 'homeOffice'
	| 'meeting';

type ExclusiveRecordType = 'exclusiveGeneral'
	| 'vacation'
	| 'sicknessBenefit'
	| 'paidVacation'
	| 'lunchBreak';

export {
	InclusiveRecordType,
	ExclusiveRecordType,
	RecordType,
}

export default RecordType;
