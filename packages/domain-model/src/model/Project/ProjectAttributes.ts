import { ProjectAssignmentAttributes, ProjectAssignment } from './ProjectAssignmentAttributes';

export interface ProjectToCreate {
	name: string;
	description: string;
	from: string;
	to: string;
	hours: number;
}

export interface Project extends ProjectToCreate {
	id: number;
	projectAssignments: ProjectAssignment[];
	hours: number;
	createdAt: string;
	updatedAt: string;
}

export interface ProjectAttributes {
	id?: number;
	projectAssignments?: ProjectAssignmentAttributes[] | null;
	name: string;
	description: string;
	from: Date;
	to: Date;
	hours: number;
	createdAt?: Date;
	updatedAt?: Date;
}
