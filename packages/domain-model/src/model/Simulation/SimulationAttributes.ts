import EmployeeAttributes from '../Employee/EmployeeAttributes';

export interface SimulationToCreate {
	employeeId: number;
	workingHoursBeginningMinutesFrom: number;
	workingHoursBeginningMinutesTo: number;
	workingHoursLengthMinutesFrom: number;
	workingHoursLengthMinutesTo: number;
	weekDaysJSONArray: string; // 0 - Sunday, 6 - Saturday
}

export interface Simulation extends SimulationToCreate {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export default interface SimulationAttributes {
	id?: number;
	employeeId?: number | null;
	contractId?: number | null;
	workingHoursBeginningMinutesFrom: number;
	workingHoursBeginningMinutesTo: number;
	workingHoursLengthMinutesFrom: number;
	workingHoursLengthMinutesTo: number;
	weekDaysJSONArray: string; // 0 - Sunday, 6 - Saturday
	createdAt?: Date;
	updatedAt?: Date;
}
