import { Column, Model, DataType, Table, HasOne, ForeignKey } from 'sequelize-typescript';
import SimulationAttributes from './SimulationAttributes';
import EmployeeModel from '../Employee/EmployeeModel';
import EmployeeAttributes from '../Employee/EmployeeAttributes';

@Table({
	tableName: 'simulation',
})
export default class SimulationModel extends Model<SimulationModel> implements SimulationAttributes {

	@Column
	workingHoursBeginningMinutesFrom: number;
	
	@Column
	workingHoursBeginningMinutesTo: number;
	
	@Column
	workingHoursLengthMinutesFrom: number;
	
	@Column
	workingHoursLengthMinutesTo: number;
	
	@Column
	weekDaysJSONArray: string;

	@ForeignKey(() => EmployeeModel)
	@Column
	employeeId: number;
}
