import { Sequelize } from 'sequelize-typescript';
import { connect as dbConnect } from 'domain-model/dist';

let sqliteConnection: Sequelize | null = null;

export async function connect() {
	sqliteConnection = await dbConnect(':memory:', false);
}

export async function close() {
	await sqliteConnection?.close();
}

export async function dropAll() {
	await sqliteConnection?.drop();
	await sqliteConnection?.dropAllSchemas({});
	await sqliteConnection?.sync();
}