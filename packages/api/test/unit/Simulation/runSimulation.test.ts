import { createNewRecordsToFitExisting } from '../../../src/Simulation/runSimulation';
import { RecordToCreate } from 'domain-model/dist/model/Record/RecordAttributes';

describe('runSimulation', () => {

	describe('createNewRecordsToFitExisting', () => {
		const employeeId = 11;
		const projectId = 22;
		const workingHoursBeginningInMinutes = 8.5*60;
		const workingHoursLengthInMinutes = 8*60;

		it('should create record from existing to the end', () => {
			const expected: RecordToCreate[] = [
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 13, 21)).toISOString(),
					to: (new Date(2020, 2, 1, 16, 30)).toISOString(),
				},
			];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: new Date(2020, 2, 1, 0, workingHoursBeginningInMinutes),
						to: new Date(2020, 2, 1, 13, 20),
					},
				],
			);

			expect(actual).toEqual(expected);
		});

		it('should create record from the beginning to the existing', () => {
			const expected: RecordToCreate[] = [
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 8, 30)).toISOString(),
					to: (new Date(2020, 2, 1, 12, 33)).toISOString(),
				},
			];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: new Date(2020, 2, 1, 12, 34),
						to: new Date(2020, 2, 1, 0, workingHoursBeginningInMinutes + workingHoursLengthInMinutes),
					},
				],
			);

			expect(actual).toEqual(expected);
		});

		it('should create record in between records', () => {
			const expected: RecordToCreate[] = [
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 14, 21)).toISOString(),
					to: (new Date(2020, 2, 1, 15, 34)).toISOString(),
				},
			];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: new Date(2020, 2, 1, 0, workingHoursBeginningInMinutes),
						to: new Date(2020, 2, 1, 14, 20),
					},
					{
						type: 'office',
						from: new Date(2020, 2, 1, 15, 35),
						to: new Date(2020, 2, 1, 0, workingHoursBeginningInMinutes + workingHoursLengthInMinutes),
					},
				],
			);

			expect(actual).toEqual(expected);
		});
	
		it('should create records around one existing', () => {
			const expected: RecordToCreate[] = [
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 8, 30)).toISOString(),
					to: (new Date(2020, 2, 1, 11, 9)).toISOString(),
				},
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 13, 21)).toISOString(),
					to: (new Date(2020, 2, 1, 16, 30)).toISOString(),
				},
			];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: new Date(2020, 2, 1, 11, 10),
						to: new Date(2020, 2, 1, 13, 20),
					},
				],
			);

			expect(actual).toEqual(expected);
		});

		it('should create records around the existing records', () => {
			const expected: RecordToCreate[] = [
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 8, 30)).toISOString(),
					to: (new Date(2020, 2, 1, 9, 0)).toISOString(),
				},
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 9, 31)).toISOString(),
					to: (new Date(2020, 2, 1, 11, 9)).toISOString(),
				},
				{
					employeeId,
					projectId,
					description: 'General work on the project',
					type: 'office',
					from: (new Date(2020, 2, 1, 13, 21)).toISOString(),
					to: (new Date(2020, 2, 1, 16, 30)).toISOString(),
				},
			];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: new Date(2020, 2, 1, 11, 10),
						to: new Date(2020, 2, 1, 13, 20),
					},
					{
						type: 'office',
						from: new Date(2020, 2, 1, 9, 1),
						to: new Date(2020, 2, 1, 9, 30),
					},
				],
			);

			expect(actual).toEqual(expected);
		});

		it('should create no record existing fills whole day', () => {
			const expected: RecordToCreate[] = [];

			const actual = createNewRecordsToFitExisting(
				new Date(2020, 2, 1),
				workingHoursBeginningInMinutes,
				workingHoursLengthInMinutes,
				employeeId,
				projectId,
				[
					{
						type: 'office',
						from: (new Date(2020, 2, 1, 8, 30)),
						to: (new Date(2020, 2, 1, 9, 0)),
					},
					{
						type: 'office',
						from: (new Date(2020, 2, 1, 9, 1)),
						to: (new Date(2020, 2, 1, 13, 20)),
					},
					{
						type: 'office',
						from: (new Date(2020, 2, 1, 13, 21)),
						to: (new Date(2020, 2, 1, 16, 30)),
					},
				],
			);

			expect(actual).toEqual(expected);
		});
	
	});

});