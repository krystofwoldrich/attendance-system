import {createServer} from 'http';
import { Sequelize } from 'sequelize-typescript';
import { connect } from 'domain-model/dist';
import initializeData from 'domain-model/dist/initialization/initData';
import initializeDataDemo from 'domain-model/dist/initialization/initDataDemo';
import {app} from './app';

let sqliteConnection: Sequelize | null = null;

const port = process.env.PORT || 4000;

(async () => {
	sqliteConnection = await connect(':memory:');
	initializeData();
	initializeDataDemo();
	createServer(app)
		.listen(
			port,
			() => console.info(`Server running on port ${port}`)
		);
})();

const stopServer = async () => {
	console.info('Server is stopping');
	await sqliteConnection?.close();
	console.log('Server stopped');
	process.exit(0);
};

process.on('SIGINT', stopServer);
process.on('SIGTERM', stopServer);
process.removeAllListeners('uncaughtException');
process.on('uncaughtException', (error: any) => console.error(error && error.stack ? error.stack : error));
process.on('unhandledRejection', (reason) => {
	console.error(reason);
});