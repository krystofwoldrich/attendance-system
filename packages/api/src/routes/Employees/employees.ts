import { Router } from 'express';
import { OK, CREATED } from 'http-status';
import EmployeeModel from 'domain-model/dist/model/Employee/EmployeeModel';
import SimulationModel from 'domain-model/dist/model/Simulation/SimulationModel';
import { simulation } from './Simulation/simulation';
import { contracts } from './Contracts/contacts';
import ContractModel from 'domain-model/dist/model/Contract/ContractModel';
import { worksheets } from './Worksheets/worksheets';

export const employees = Router();

employees.post('/', async (req, res, next) => {
	try {
		const employee = await EmployeeModel.create(req.body);
		await employee.reload({include: [SimulationModel, ContractModel]});
		res.status(CREATED).send(employee.toJSON());
	} catch (error) {
		next(error);
	}
});

employees.get('/', async (req, res, next) => {
	try {
		const employees= await EmployeeModel.findAll({include: [SimulationModel, ContractModel]});
		res.status(OK).send(employees);
	} catch (e) {
		next(e);
	}
});

employees.use('/', simulation);
employees.use('/', contracts);
employees.use('/', worksheets);
