import { Router } from 'express';
import * as pdf from 'pdfjs';
import { Op } from 'sequelize';
import * as moment from 'moment';
import EmployeeModel from 'domain-model/dist/model/Employee/EmployeeModel';
import ProjectModel from 'domain-model/dist/model/Project/ProjectModel';
import { NOT_FOUND, BAD_REQUEST } from 'http-status';
import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import mapRecordsToMonthAndDate from '../../../Records/mapRecordsToMonthAndDate';
import getRecordsLengthInMinutes from '../../../Records/getRecordsLenghtInMinutes';
import * as FontCourier from 'pdfjs/font/Courier';
import RecordsMapByDate from '../../../Records/RecordsMapByDate';

//https://stackoverflow.com/questions/12756159/regex-and-iso8601-formatted-datetime
const ISO_8601 = /^\d{4}(-\d\d(-\d\d(T\d\d:\d\d(:\d\d)?(\.\d+)?(([+-]\d\d:\d\d)|Z)?)?)?)?$/i;

const MONTHS = ['Led', 'Úno', 'Bre', 'Dub', 'Kve', 'Cer', 'Cec', 'Srp', 'Zar', 'Ríj', 'Lis', 'Pro'];

const TITLE_FONT_SIZE = 25;
const HEADING_1_FONT_SIZE = 18;
const HEADING_2_FONT_SIZE = 16;

const TEXT_HEADING_1_OPTIONS = {
	fontSize: 18,
};

const TEXT_HEADING_2_OPTIONS = {
	fontSize: 14,
};

export const worksheets = Router();

const defaultRowOptions = {
	minHeight: 10,
};

worksheets.post('/:employeeId/worksheets', async (req, res, next) => {
	try {
		const { body } = req;

		const errors: string[] = [];
		/* tslint:disable curly */
		if (!req.body.projectId) errors.push('Missing "projectId" in the request body.');
		if (!ISO_8601.test(body.from)) errors.push('Not valid "from" in the request body. Please use ISO 8601 form.');
		if (!ISO_8601.test(body.to)) errors.push('Not valid "to" in the request body. Please use ISO 8601 form.');
		/* tslint:enable curly */
		if (errors.length > 0) {
			return res.status(BAD_REQUEST).send(errors.join('\n'));
		}

		const employeeId: number = parseInt(req.params.employeeId);
		const projectId: number = body.projectId;
		const from: Date = new Date(body.from);
		const to: Date = new Date(body.to);

		const employee = await EmployeeModel.findOne({where: { id: employeeId }});
		const project = await ProjectModel.findOne({where: { id: projectId }});
		const records = await RecordModel.findAll({
			where: {
				projectId,
				employeeId,
				from: {
					[Op.gte]: from,
					[Op.lte]: to,
				},
			},
			order: [
				['from', 'ASC'],
			],
		});
		const recordsMapByMonthAndDate = mapRecordsToMonthAndDate(records);

		if (employee === null) {
			return res.status(NOT_FOUND).send();
		}
		if (project === null) {
			return res.status(BAD_REQUEST).send('Employee is not part of the given project.');
		}

		const doc = new pdf.Document({
			height: 595.296,
			width: 841.896,
			font: FontCourier,
		});
		const filenameCandidate = `${employee.name}_${project.name}`;
		const filename = encodeURIComponent(filenameCandidate) + '.pdf';
		res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"')
		res.setHeader('Content-type', 'application/pdf')

		doc.cell({ paddingBottom: 0.5*pdf.cm }).text('Jméno: ' + employee.name, TEXT_HEADING_1_OPTIONS);
		doc.cell({ paddingBottom: 0.5*pdf.cm }).text('Projekt: '+ project.name, TEXT_HEADING_2_OPTIONS);
		
		let recordsTable = doc.table({
			widths: new Array(32).fill(null),
			borderWidth: 1,
		});

		recordsTable = prepareRecordTableDatesRow(recordsTable);

		let month = 0;
		recordsMapByMonthAndDate.forEach((recordsMapByDate) => {
			const monthRecordRow = recordsTable.row(defaultRowOptions);
			
			monthRecordRow.cell(MONTHS[month]);

			recordsMapByDate.forEach((dayRecords) => {
				const dayRecordsLength = getRecordsLengthInMinutes(dayRecords);

				const toRenderInCell = dayRecordsLength === 0 ? undefined : (dayRecordsLength/60).toFixed(1);
				monthRecordRow.cell(toRenderInCell);
			});
			month++;
		});

		doc.pipe(res);
		doc.end();
	} catch (error) {
		next(error);
	}
});

export function prepareRecordTableDatesRow(table: pdf.Table): pdf.Table {
	const dateRow = table.row(defaultRowOptions);

	dateRow.cell('');

	for (let index = 1; index < 32; index++) {
		dateRow.cell(index.toString());
	}

	return table;
}
