import { Router } from 'express';
import { OK, CREATED, NOT_FOUND } from 'http-status';
import SimulationModel from 'domain-model/dist/model/Simulation/SimulationModel';
import runSimulation from '../../../Simulation/runSimulation';

export const simulation = Router();

simulation.post('/:employeeId/simulation/run', async (req, res, next) => {
	try {
		const { body } = req;
		const employeeId = parseInt(req.params.employeeId);
		const projectAssignmentId = parseInt(body.projectAssignmentId);
		const from = new Date(body.from);
		const to = new Date(body.to);
		
		if (employeeId === NaN) {
			throw new Error(`employeeId must be an integer: Got: ${req.params.employeeId}`);
		}
		if (projectAssignmentId === NaN) {
			throw new Error(`projectAssignmentId must be an integer: Got: ${body.projectAssignmentId}`);
		}
		if (Date.parse(body.from) === NaN) {
			throw new Error(`from must be an ISO string parsable by JS: Got: ${body.from}`);
		}
		if (Date.parse(body.to) === NaN) {
			throw new Error(`from must be an ISO string parsable by JS: Got: ${body.to}`);
		}

		await runSimulation({
			employeeId,
			projectAssignmentId,
			from,
			to,
		});

		res.status(OK).send();
	} catch (error) {
		next(error);
	}
});

simulation.post('/:employeeId/simulation', async (req, res, next) => {
	try {
		const { body } = req;
		const employeeId = req.params.employeeId;

		const simulationToCreate = { ...body,  employeeId };
		const simulation = await SimulationModel.create(simulationToCreate);
		res.status(CREATED).send(simulation);
	} catch (error) {
		next(error);
	}
});

simulation.get('/:employeeId/simulation', async (req, res, next) => {
	try {
		const employeeId = req.params.employeeId;
		const simulation = await SimulationModel.findOne({where: { employeeId }});
		if (simulation) {
			res.status(OK).send(simulation);
		} else {
			res.status(NOT_FOUND).send();
		}
	} catch (error) {
		next(error);
	}
});
