import { Router } from 'express';
import { OK, CREATED } from 'http-status';
import ProjectAssignmentModel from 'domain-model/dist/model/Project/ProjectAssignmentModel';
import { ProjectAssignmentAttributes } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

export const projectAssignments = Router();

projectAssignments.post('/:projectId/assignments', async (req, res, next) => {
	try {
		const { body } = req;
		const projectId = req.params.projectId;
		const projectAssignmentToCreate: ProjectAssignmentAttributes = { ...body, projectId };
		const projectAssignment = await ProjectAssignmentModel.create(projectAssignmentToCreate);
		res.status(CREATED).send(projectAssignment);
	} catch (error) {
		next(error);
	}
});

projectAssignments.get('/:projectId/assignments', async (req, res, next) => {
	try {
		const projectId = req.params.projectId;
		const projectAssignments = await ProjectAssignmentModel.findAll({ where: { projectId } });
		res.status(OK).send(projectAssignments);
	} catch (error) {
		next(error);
	}
});
