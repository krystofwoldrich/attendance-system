import RecordsMapByDate from './RecordsMapByDate';

type RecordsMapByMonthAndDate = RecordsMapByDate[];

export default RecordsMapByMonthAndDate;
