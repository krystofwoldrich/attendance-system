import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import RecordsMapByMonth from './RecordsMapByMonth';


export default function mapRecordsToMonth(records: RecordModel[]): RecordsMapByMonth {
	const emptyMap: RecordsMapByMonth = [];
	for (let index = 0; index < 12; index++) {
		emptyMap.push([]);
	}

	return records.reduce<RecordsMapByMonth>(
		(map, record) => {
			map[record.from.getMonth()].push(record);
			return map;
		},
		emptyMap,
	);
}
