import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import RecordsMapByMonthAndDate from './RecordsByMonthAndDate';
import mapRecordsToMonth from './mapRecordsToMonth';
import RecordsMapByDate from './RecordsMapByDate';
import mapRecordsToDate from './mapRecordsToDate';


export default function mapRecordsToMonthAndDate(records: RecordModel[]): RecordsMapByMonthAndDate {
	const emptyMap: RecordsMapByMonthAndDate = [];
	for (let index = 0; index < 12; index++) {
		const emptyDateMap = [];
		for (let dateIndex = 0; dateIndex < 31; dateIndex++) {
			emptyDateMap.push([]);
		}
		emptyMap.push(emptyDateMap);
	}

	const recordsMapByMonth = mapRecordsToMonth(records);
	
	return recordsMapByMonth.map<RecordsMapByDate>((monthRecords) => {
		return mapRecordsToDate(monthRecords);
	});
}
