import RecordModel from 'domain-model/dist/model/Record/RecordModel';

type RecordsMapByDate = RecordModel[][];

export default RecordsMapByDate;
