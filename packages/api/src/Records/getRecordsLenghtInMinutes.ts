import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import getRecordLengthInMinutes from './getRecordLengthInMinutes';

export default function getRecordsLengthInMinutes(records: RecordModel[]): number {
	let total = 0;

	records.forEach((record) => {
		total = total + getRecordLengthInMinutes(record);
	});

	return total;
}
