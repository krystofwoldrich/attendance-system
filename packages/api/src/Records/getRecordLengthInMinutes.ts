import RecordModel from 'domain-model/dist/model/Record/RecordModel';

export default function getRecordLengthInMinutes(record: RecordModel): number {
	const recordLengthInMs = record.to.valueOf() - record.from.valueOf();
	return ( recordLengthInMs / 1000 ) / 60;
}
