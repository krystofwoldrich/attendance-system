import { Application } from 'express';
import { employees } from './routes/Employees/employees';
import { projects } from './routes/Projects/projects';
import { records } from './routes/Records/records';

export default function(app: Application) {
	app.use('/employees', employees);
	app.use('/projects', projects);
	app.use('/records', records);
}
