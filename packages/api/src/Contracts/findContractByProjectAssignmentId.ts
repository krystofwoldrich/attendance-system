import ContractModel from 'domain-model/dist/model/Contract/ContractModel';
import { ContractAttributes } from 'domain-model/dist/model/Contract/ContractAttributes';
import ProjectAssignmentModel from 'domain-model/dist/model/Project/ProjectAssignmentModel';

export default async function findContractByProjectAssignmentId(
    projectAssignmentId: number,
): Promise<ContractAttributes | null> {
    const projectAssignment = await ProjectAssignmentModel.findOne({ where: { id: projectAssignmentId } });
    const contract = projectAssignment
        ? ContractModel.findOne({ where: { id: projectAssignment.contractId } })
        : null;
    
    return contract;
}
