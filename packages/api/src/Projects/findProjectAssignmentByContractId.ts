import { ProjectAssignmentAttributes } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

export default function findProjectAssignmentByContractId(
	assignments: ProjectAssignmentAttributes[],
	contractId: number
): ProjectAssignmentAttributes | undefined {
	return assignments.find((item) => item.contractId === contractId);
}
